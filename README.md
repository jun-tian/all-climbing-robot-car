# all-climbing-robot-car

#### 介绍
all-climbing-robot-car.ino爬墙机器人arduino主控，蓝牙控制
使用Arduino，蓝牙和Android app的爬墙机器人 - 在本教程中，您将学习如何使用基于Atmega328微控制器的定制控制器板制作轻巧，低成本和高效的爬墙机器人，这与Arduino Uno，HC-05或HC-06蓝牙模块中使用的微控制器相同， L298N电机驱动器，6v迷你直流减速电机和高转速四

#### 软件架构
![输入图片说明](https://images.gitee.com/uploads/images/2022/0218/145849_4dd8d78d_10358160.jpeg "wall-climbing-robot-3d-model.jpg")


#### 安装教程

![输入图片说明](https://images.gitee.com/uploads/images/2022/0218/145902_defddef9_10358160.png "Wall-Climbing-Robot-Circuit-Diagram.png")

#### 使用说明

物料准备：

6v 60RPM Mini Dc Gear Motors

Atmega328 micro controller:

16Mhz crystal:

22pf capacitors:

10k Resistor:

10uf capacitors:

L298N motor driver:

1000KV Brushless Motor, 30A Speed controller, and Propellers

Top Brushless Motors

3s Lipo Batteries

4s Lipo Batteries

Bluetooth Module: Hc-05:

Arduino Uno

Arduino Nano


#### 参与贡献



#### 特技


